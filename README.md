Setup

_**Note: tested in IntelliJ IDEA CE only!**_
* Checkout project, import to IDE
* Let Gradle sync, run _tests.IdmAcceptanceTest_
* If you want to run with Chrome, add `-Dselenide.browser=chrome` to VM options
* If you get an error about webdriver.chrome.driver not being found, add `-Dwebdriver.chrome.driver=<path to webdriver>` to you VM options

_**Gradle Options (running from command line)**_
* `-Pselenide.browser=<chrome|phantomjs>` browser to run tests in
* `-Pidm.user=<IDM user name>` Active Grid username
* `-Pidm.password=<IDM user password>` Grid password
* `-Pwebdriver.chrome.driver=<path to Chrome webdriver>` Path to chrome webdriver, when tests are run locally
* `-PbaseUrl=<url>` Base URL of the running IDM instance
* `-Premote=<URL of remote webdriver>` URL of the remote Selenium server, when tests run against remote machine

__**Run in docker**__  
>_When run in container, there's no need for local Chrome browser or webdriver_

_**Note:** This part assumes usage of public containers from Docker Hub. Whether it makes sense to build our own containers is still to be discussed._ 
* Find suitable container. I'll be using docker selenium/standalone-chrome from [Docker Hub](https://hub.docker.com) for this readme.
* Run `docker run --dns<company DNS server> -d -P selenium/standalone-chrome` DNS server parameter is needed to allow the container to find the company network
* Find ID if the running container by running `docker ps`
* Use the ID to find URL of the Selenium server exposed by the container `docker port <container ID> 4444`
* Run the tests specifying the remote URL found in previous step: `gradlew test -Pselenide.browser=chrome -Pidm.user=<IDM user name> -Pidm.password=<IDM user password> -Premote=<remote URL>`
