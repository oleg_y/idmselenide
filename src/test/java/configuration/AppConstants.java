package configuration;

public class AppConstants {
    public static final String BASE_URL = "https://destvwidmqa.infor.com:33000";
    public static final String CLIENT_URL = "/ca/client/index.html";
    public static final String LOGIN_URL = "/grid/login";
    public static final int DEFAULT_TIMEOUT = 2000;
    public static final String DOCUMENT_MANAGEMENT_TITLE = "Document Management";
}
