package configuration;

public class DocumentConfig {

    public static final String ENTITY_TYPE = "Document Management";
    public static final String LOCATION = "Prague";
    public static final String NAME = "Test";
    public static final String ID1 = "Testing";
    public static final String ID2 = "Testing1";
    public static final String FILE_PATH = "C:\\Users\\pgill\\Repository\\IDM_Acceptance\\idmselenide\\src\\test\\resources\\Upload\\1.jpg";
    public static final String FILE_PATH1 = "C:\\Users\\pgill\\Repository\\IDM_Acceptance\\idmselenide\\src\\test\\resources\\Upload\\2.jpg";
    public static final String USER = "Irdcmfiladm";
}

