package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AddDocumentPage;
import pages.SearchPage;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.page;

public class AddDocumentStepdefs {
    @When("^User clicks on Add Document Button$")
    public void userClicksOnAddDocumentButton() throws Throwable {
        AddDocumentPage page = page(AddDocumentPage.class);
        page.addDocument();
    }

    @Then("^User selects document type ([^\"]*)$")
    public void userSelectsDocumentTypeDocType(String docType) throws Throwable {
        AddDocumentPage page = page(AddDocumentPage.class);
        page.selectDocumentType(docType);
    }

    @Then("^User verifies Blank Template window and clicks on Create Button$")
    public void userVerifiesBlankTemplateWindowAndClicksOnCreateButton() throws Throwable {
        AddDocumentPage page = page(AddDocumentPage.class);
        page.verifyTemplateWindowAndCreateNew();
    }

    @And("^User fills the fields and add a file ([^\"]*)$")
    public void userFillsTheFieldsAndAddAFileDocType(String docType) throws Throwable {
        page(AddDocumentPage.class).waitTillElementVisible();
        AddDocumentPage page = page(AddDocumentPage.class);
        switch (docType) {
            case "Document":
                page.fillDocumentTypeFields();
                page.switchToFileTab();
                page.uploadFileToDocument();
                page.saveDocument();
                page.verifyImageExists();
                break;
            case "File":
                page.fillFileFields();
                page.switchToFileTab();
                page.uploadFile();
                page.saveDocument();
                page.verifyImageExists();
        }

    }

    @And("^User searches for added document ([^\"]*)$")
    public void userSearchesForAddedDocument(String docType) throws Throwable {
      AddDocumentPage page = page(AddDocumentPage.class);
      page.switchToDocumentTab();
        SearchPage searchPage=page(SearchPage.class);
        searchPage.selectDocumentType(docType);
        searchPage.selectAttribute();
        searchPage.selectOperation();
        searchPage.selectUser();
        searchPage.search();
        searchPage.searchResult();
        searchPage.verifyRows();

    }
}
