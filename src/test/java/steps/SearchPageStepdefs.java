package steps;

import com.codeborne.selenide.Selenide;
import configuration.AppConstants;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AddDocumentPage;
import pages.AdvancedSearchPage;
import pages.CommonInAllPages;
import pages.SearchPage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;

@SuppressWarnings("unused")
public class SearchPageStepdefs {


    @Given("^User is on index page$")
    public void user_is_on_index_page() throws Throwable {
        open(AppConstants.BASE_URL + AppConstants.CLIENT_URL);
        Selenide.page(CommonInAllPages.class).getTitle();
    }

    @Given("^User is on Search Page$")
    public void userIsOnSearchPage() throws Throwable {
        page(SearchPage.class).waitTillRendered();
    }

    @When("^User searches by document type ([\\S_]+)$")
    public void user_selects_document_type_from_dropdown_list(String docType) throws Throwable {
        SearchPage page = page(SearchPage.class);
        page.selectDocumentType(docType);
        page.search();
    }

    @When("^Search container is displayed$")
    public void Search_container_is_displayed() throws Throwable {
        SearchPage page = page(SearchPage.class);
        page.searchContainer();
    }

    @Then("^User Searches by clicking on Search Button$")
    public void userSearchesByClickingOnSearchButton() throws Throwable {
        SearchPage page = page(SearchPage.class);
        page.search();
    }

    @And("^no result is displayed$")
    public void noResultIsDisplayed() throws Throwable {
        SearchPage page = page(SearchPage.class);
        page.searchNoResult();
    }

    @And("^result is displayed$")
    public void resultIsDisplayed() throws Throwable {
        page(CommonInAllPages.class).waitForLoadingIndicator();
        SearchPage page = page(SearchPage.class);
        page.searchResult();
      //  String message=Selenide.page(SearchPage.class).searchResult();

    }

    @Then("^XQuery changes to (\\/[\\S_]+)$")
    public void xquery_changes_to_selected_document_type(String docType) throws Throwable {
        String xQueryString = Selenide.page(AdvancedSearchPage.class).getSearchString();
        if (!xQueryString.equals(docType)) {
            throw new Exception(String.format("XQuery doesn't match, expected %s, found %s", docType, xQueryString));
        }
    }

    @Then("^User clicks on Shortcut Dropdown$")
    public void userSearchesByShortcutMenu() throws Throwable {
        AdvancedSearchPage page = page(AdvancedSearchPage.class);
        page.clickOnShortcutDropdown();
    }

    @Then("^User searches by clicking on ([^\"]*)$")
    public void userSearchesByClickingOn(String shortCut) throws Throwable {
        AdvancedSearchPage page = page(AdvancedSearchPage.class);
        page.selectShortcutLink(shortCut);
    }
}

