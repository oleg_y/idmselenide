package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import configuration.DocumentConfig;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage extends CommonInAllPages {
    public void waitTillRendered() {
        $(By.tagName("idm-attribute-search")).waitUntil(Condition.exist, 5000);
    }

    public void selectDocumentType(String documentType) {
        $(By.name("ddlEntities")).parent().$(By.cssSelector("div.dropdown")).click();
        for (SelenideElement docType: $$(By.className("dropdown-option"))) {
            if (docType.getText().equals(documentType)) {
                docType.click();
                break;
            }
        }
    }

    public void selectAttribute() {
     SelenideElement option=   $(By.name("ddlAttributes")).parent().$(By.cssSelector("div.dropdown"));
     option.click();
        SelenideElement dropdownOption= $(By.cssSelector(".dropdown-option"),3);
        dropdownOption.click();
        option.shouldHave(Condition.matchText("CreatedBy"));
    }

    public void selectOperation() {
        SelenideElement operationOption=   $(By.name("ddlOperators")).parent().$(By.cssSelector("div.dropdown"));
        operationOption.click();
        SelenideElement dropdownOption= $(By.cssSelector(".dropdown-option"),1);
        dropdownOption.click();
//        operationOption.shouldHave(Condition.matchText("=Equal"));
    }

    public void selectUser() {
        SelenideElement user= $(By.cssSelector(".searchfield-wrapper input"),1).waitUntil(Condition.exist, 300);
        user.click();
   //     user.clear();
        user.setValue(DocumentConfig.USER);
    }

    public void verifyRows() {
        SelenideElement rows= $(By.cssSelector("idm-card-list .listview > ul li"),0).waitUntil(Condition.exist, 300);
        rows.getSize();
        rows.isDisplayed();
    }

    public void search() {
        $(By.cssSelector("idm-attribute-search > div.row.buttons > button.btn-primary")).click();
    }

    public void searchContainer() { $(By.className("search-container")).isDisplayed();}

    public void searchNoResult() throws Exception {
        $(By.className("search-count-area"))
                .shouldHave(Condition.matchText("^[0]\\sdocuments matching your search$"));
    }

    public void searchResult() {
        $(By.className("search-count-area"))
                .shouldHave(Condition.matchText("^[0-9]{1,9}\\sdocuments matching your search$"));
    }

}
