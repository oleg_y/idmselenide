package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.gargoylesoftware.htmlunit.javascript.host.intl.DateTimeFormat;
import configuration.DocumentConfig;
import org.openqa.selenium.By;

import java.util.Date;

import static com.codeborne.selenide.Selenide.*;

public class AddDocumentPage extends CommonInAllPages {

    public void addDocument() {
        $(By.xpath("//span[text()='Add Document']")).click();
    }

    public void selectDocumentType(String documentType) {
        $(By.className("add-document-container")).parent();
        for (SelenideElement docType : $$(By.className("documentTypes"))) {
            if (docType.getText().equals(documentType)) {
                docType.click();
                break;
            }
        }
    }

    public void verifyTemplateWindowAndCreateNew() {
        SelenideElement templateWindow = $(By.className("is-selected"));
        if (templateWindow.exists()) {
            SelenideElement createButton = $(By.xpath("//button[text()='Create']"));
            createButton.click();
        }
    }

    public void waitTillElementVisible() {
        $(By.xpath("//span[text()='Save']")).waitUntil(Condition.exist, 5000);
    }

    public void fillDocumentTypeFields() {
        $(By.cssSelector("idm-form-field input"), 0).setValue(DocumentConfig.ENTITY_TYPE);
        $(By.cssSelector("idm-form-field input"), 2).setValue(DocumentConfig.LOCATION);
        Date date= new Date();
        $(By.cssSelector("idm-form-field input"), 3).setValue(DocumentConfig.ID1+date.getDate());
    }

    public void fillFileFields() {
        Date date= new Date();
        $(By.cssSelector("idm-form-field input"), 0).setValue(DocumentConfig.NAME+date.getDate());
       SelenideElement dropdown=$(By.cssSelector(".dropdown-wrapper > .dropdown"));
       dropdown.click();
       SelenideElement dropdownOption= $(By.cssSelector(".dropdown-option"),6);
       dropdownOption.click();
    }

    public void switchToFileTab() {
        SelenideElement fileTab = $(By.xpath("//a[text()='File']"));
        fileTab.isDisplayed();
        fileTab.click();
        fileTab.isSelected();
    }

    public void switchToDocumentTab() {
        SelenideElement fileTab = $(By.xpath("//a[text()='Documents']"));
        fileTab.isDisplayed();
        fileTab.click();
        fileTab.isSelected();
    }

    public void uploadFile(){
        SelenideElement fileUpload=$(By.id("file-name"));
        fileUpload.sendKeys(DocumentConfig.FILE_PATH1);
    }

    public void uploadFileToDocument(){
        SelenideElement fileUpload=$(By.id("file-name"));
        fileUpload.sendKeys(DocumentConfig.FILE_PATH);
    }

    public void saveDocument() {
        $(By.xpath("//span[text()='Save']")).click();
        page(CommonInAllPages.class).waitForLoadingIndicator();
    }

    public void verifyImageExists(){
        SelenideElement image = $(By.className("is-selected"));
        image.exists();
        SelenideElement fileName = $(By.className("filename"));
        fileName.shouldHave(Condition.matchText("(1\\.jpg|2\\.jpg)"));
    }
}

