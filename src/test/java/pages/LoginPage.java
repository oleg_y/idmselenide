package pages;

import configuration.AppConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {
    public void login() {
        open(AppConstants.BASE_URL + AppConstants.LOGIN_URL);

        if (!isLoggedIn()) {
            $(By.id("userId")).setValue(System.getProperty("idm.user"));
            $(By.id("password")).setValue(System.getProperty("idm.password"))
                .pressEnter();
        }
    }

    private boolean isLoggedIn() {
        try {
            $(By.id("userId"));
            return false;
        } catch (NoSuchElementException ex) {
            return true;
        }
    }
}
