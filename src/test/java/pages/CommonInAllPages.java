package pages;

import com.codeborne.selenide.Condition;
import configuration.AppConstants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static com.codeborne.selenide.Selenide.$;

public class CommonInAllPages {

    public void getTitle() {
        String searchText = $(By.className("title")).getText();
        Assert.assertTrue(searchText.matches(AppConstants.DOCUMENT_MANAGEMENT_TITLE));
    }

    public void waitForLoadingIndicator() {
        try {
            if( $(By.className("busy-indicator")).exists()){
          //  $(By.className("busy-indicator"))
            //        .waitUntil(Condition.exist, AppConstants.DEFAULT_TIMEOUT);
            $(By.className("busy-indicator"))
                    .waitUntil(Condition.disappear, AppConstants.DEFAULT_TIMEOUT);}
        } catch (NoSuchElementException ex) {
            // pass
        }
    }
}
