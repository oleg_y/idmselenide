package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class AdvancedSearchPage extends CommonInAllPages {

    public String getSearchString() {
        if (!$(By.name("advancedSearchTextarea")).exists()) {
            expandAdvancedSearch();
        }
        return $(By.name("advancedSearchTextarea")).val();
    }

    private void expandAdvancedSearch() {
        $(By.cssSelector("idm-advanced-search div.arrow-right-align > button")).click();
    }

    public void clickOnShortcutDropdown() {
        expandShortcutMenu();
        verifyLinksPresent();
    }

    private void expandShortcutMenu() {
        $(By.cssSelector("idm-shortcuts div.arrow-right-align > button")).click();

    }

    private void verifyLinksPresent() {
        SelenideElement CheckedOutByMe = $(By.linkText("Checked out by me"));
        CheckedOutByMe.isDisplayed();
        SelenideElement CreatedByMe = $(By.linkText("Created by me"));
        CreatedByMe.isDisplayed();
    }

    public void selectShortcutLink(String shortCut) {
        SelenideElement ShortCutName = $(By.linkText(shortCut));
        ShortCutName.isDisplayed();
        ShortCutName.click();
    }
}