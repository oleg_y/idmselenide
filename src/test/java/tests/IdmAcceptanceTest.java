package tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import pages.LoginPage;

import static com.codeborne.selenide.Selenide.page;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"src/test/resources/features"},
        glue = "steps",
        tags = {"~@ignore","@addDocument"},
        format = "pretty")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IdmAcceptanceTest {

    @Before
    public void setUp() {
    }

    @BeforeClass
    @SuppressWarnings("unused")
    public static void beforeClass() {
        page(LoginPage.class).login();
    }

    @AfterClass
    public static void tearDown() {
    }
}
