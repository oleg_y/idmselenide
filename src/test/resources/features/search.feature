Feature: Search document

  Background:
    Given User is on index page

  @searchNoResult
  Scenario: Search Page is displayed
    Given User is on Search Page
    When Search container is displayed
    Then User Searches by clicking on Search Button
    And no result is displayed

  @searchResult
  Scenario Outline: User selects a document type and result is displayed
    Given User is on Search Page
    When Search container is displayed
    Then User searches by document type <docType>
    And XQuery changes to <docTypeQuery>
    And result is displayed
    Examples:
      | docType     | docTypeQuery |
    #  | All_players | /All_players |
      | DocTemplate | /DocTemplate |
      | File        | /MDS_File    |

  @searchViaShortcut
  Scenario Outline: User Searches by ShortCut Links
    Given User is on Search Page
    When Search container is displayed
    Then User clicks on Shortcut Dropdown
    Then User searches by clicking on <shortCut>
    And result is displayed
    Examples:
      | shortCut          |
      | Checked out by me |
      | Created by me     |

