Feature: Add document

  Background:
    Given User is on index page

  @addDocument
  Scenario Outline: New Document is added
    Given User is on Search Page
    When User clicks on Add Document Button
    Then User selects document type <docType>
    Then User verifies Blank Template window and clicks on Create Button
    And User fills the fields and add a file <docType>
    And User searches for added document <docType>
    Examples:
      | docType  |
      | Document |
      | File     |

